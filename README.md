# Docker Nginx + Log Management

#### Problem Statement:
Deploy a Dockerized application serving a static website via (e.g. via Nginx) that displays
a custom welcome page (but not the default page for the web server used)
- Use fluentd to ship Nginx request logs to an output of your choice (e.g. S3,ElasticSearch)
- Provide a standalone solution including both webapp and fluentd using docker-compose and/or a kubernetes deployment (plain manifest or helm chart)

#### Solutions Details: (Demo Video Link: https://streamable.com/jmvs5)
- Created docker-compose.yml to deploy and run nginx and fluentd containers.
- A default HTML page of nginx is replaced with our own custom page that prints a single line "Nginx Log Management"
- There is es.env file in root directory of project to specify the elasticsearch host and port information
- To deploy and run services, use command `docker-compose up -d`. Make sure to update es.env file with hostname and port information of elasticsearch
- Nginx access log pattern supported (taken from nginx.conf)
```
log_format  main  '$remote_addr - $remote_user [$time_local] "$request" ' '$status $body_bytes_sent "$http_referer" ' '"$http_user_agent" "$http_x_forwarded_for"';
```